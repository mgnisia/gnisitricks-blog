#include "iostream"

class Airplane {
   public:
    int number;
    float v_max;
    float v_min;
    std::string airline;
    Airplane(int num, float vmax, float vmin, std::string name): number(num), v_max(vmax),
    v_min(vmin), airline(name) {
        std::cout << "An Instance of the airpline has been created!" << std::endl;
    };
    void speeds() {
        std::cout << "The minimum speed is " << v_min << std::endl;
        std::cout << "The maximum speed is " << v_max << std::endl;
    };
};

int main(int argc, char const *argv[])
{
    Airplane a_1(2, 550, 120, "Fly me to the moon");
    a_1.speeds();
    return 0;
}
