---
title:  "Vektorfelder in Matlab"
categories: ['Matlab']
tags: ['Vektorfelder', 'Visualisierung']
author: "Moritz Gnisia"
date: 2018-11-25
hasmath: true
type: docs
---

Die Darstellung von Vektorfelder erleichtert häufig das Verständnis z.B. von Wachstumsprozessen oder ähnlichen konvergten Prozess. 

<!--more-->

Mit folgendem Code lassen sich diese einfach darstellen. Das Wichtige dabei ist, dass die Steigung in diesem Fall normiert aufgetragen wird. Die Normierung erfolgt
über den Befehl `ones`.

``` matlab
clear;
clf; 
%  creates grid points in the tp-plane 
[t, p]= meshgrid(-10:0.5:20,-1:.5:12);
r = 0.2;
k = 7;
l = 2;
slope = -r.*(1-(2*p)/k).*(1-p/(2*l));      
%  plots direction vectors at all grid 
quiver(t,p,ones(size(t)),slope)
```

In diesem Fall wird folgende Gleichung dargestellt:

$$\dot{p} = -r (1 - \frac{2p}{k})( 1 - \frac{p}{2l})$$

Man erhält folgende Darstellung:

![nginx](/vektorfeld.png)
