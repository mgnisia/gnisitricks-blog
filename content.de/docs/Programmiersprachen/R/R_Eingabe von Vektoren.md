---
title:  "Eingabe und Aufrufen von Vektoren in R"
date: 2018-09-03T22:09:29+01:00
draft: false
---


### Eingabe von Vektoren

Vektoren sind vermutlich zu Beginn die einfachste Möglichkeit Daten in R bzw. RStudio einzugeben. Die Syntax ist hierbei wie folgt aufgebaut:
``` R
v1 <- c(1,2,3,4,5)
```
<!--more-->

`v1` ist in diesem Fall unsere Variable, der wir den Vektor `(1,2,3,4,5)` zuweisen. Die Zuweisung erfolgt über `<-`.

Update Vektoren können aber nicht nur aus Zahlen wie in dem Beispiel sein, sondern ebenfalls aus Buchstabenketten/Wörter (engl. Strings) bestehen. Ein weiteres Beispiel sind sogenannten boolsche Werte also `True/False`. Hierfür nun zwei Beispiele:

``` R
wort <- c('Beispiel1','Beispiel2','Beispiel3')
bool <- c(TRUE, FALSE, TRUE)
```

### Aufruf von Vektoren

Anschließend können wir die Variable über die Eingabe des Wort `v1` in das Terminal aufrufen. Möchten wir nur bestimmte Elemente des Vektors aufrufen, so geht des über folgenden Befehl:

``` R
# Aufruf des ersten Elements des Vektors v1
v1[1]
# Aufruf des zweiten Elements des Vektors v1
v1[2]
```

Allerdings können nicht nur bestimmte Elemente aufgerufen werden, sondern auch mehrere z.B.

``` R
# Aufruf des ersten zwei Elemente des Vektors v1
v1[1:2]
```
