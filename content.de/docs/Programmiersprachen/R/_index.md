---
bookCollapseSection: true
weight: 20
---

# R Einträge

## Funktionen

- [**Eingabe von Vektoren**]({{< ref "R_Eingabe%20von%20Vektoren.md" >}})
- [**Rechnen mit Vetkoren**]({{< ref "R_Rechnen%20mit%20Vektoren.md" >}})