---
layout: single
title:  "Rechnen in R"
categories: ['R']
tags: ['Vektoren', 'Einführung']
author: "Moritz Gnisia"
date: 2018-09-03
---
Nachdem wir bereits einfache Vektoren in R eingegeben haben (s. [hier]), können wir mit diesen sehr einfach rechnen.

<!--more-->

Zu Beginn definieren wir zwei Vektoren, die die Rechenoperationen verdeutlichen sollen:

``` R
v1 <- c(3,4,6,9,11)
v2 <- c(16,10,4,2,1)
```

### einfache Rechenoperationen

``` R
# Addition
add<-v1+v2
# Subtraktion
sub<-v1-v2
# Multiplikation
mult<-v1*v2
# Division
div<-v1/v2
```
Wichtig ist hierbei, dass die Multiplikation elementweise erfolgt und für die Division die Länge der Vektoren identisch sein muss. Um zu überprüfen, ob die Länge von zwei Vektoren identisch ist, kann man sich die Länge mit dem Befehl `length` ausgeben lassen.

``` R
# Ausgabe der Länge eines Vektors
length(v1)
# Vergleich der Länge von zwei Vektoren, bei Eingabe des folgenden Befehls, solltet ihr den Wert TRUE erhalten.
length(v1) == length(v2)
```
