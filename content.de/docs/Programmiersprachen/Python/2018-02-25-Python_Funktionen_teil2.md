---
title:  "Erweiterte Funktionen"
categories: ['Python']
tags: ['Funktion', 'Einführung']
author: "Moritz Gnisia"
date: 2018-09-03
draft: false
---


Welche Möglichkeiten bestehen Funktionen zu erweitern?

Zunächst einmal gilt es zu unterscheiden, welche Parametertypen für eine Funktion grundsätzlich möglich sind. Diese kann man der folgenden Tabelle entnehmen.

### Theorie


|    Typ   |   Erläuterung          |
|:---------:|-------------|
| Pflichtparameter    | Diese Parameter müssen bei Aufruf der Funktion vorgegeben werden.  |
| Standardparameter      | Diese Parameter können bei Aufruf der Funktion vorgegeben werden. Falls dies nicht der Fall ist, wird der vordefinierte Fall des Parameters angenommen.   |
| Parameter mit unterschiedlicher Länge `(*args)`   | Von diesen Parametern (arguments) können beliebig viele an die Funktion übergeben werden.        |
| Schlüsselparameter (`**kwargs`)       | Mit diesen Parametern (keyword-arguments)  können Optionen der Funktion gesteuert werden.     |

Damit die verschiedenen Parametertypen deutlich werden, folgen nun zu jedem Typ ein Beispiel.


### Pflichtparameter

Bereits im Post [einfache Funktionen] haben wir eine Funktion mit Pflichtparametern geschrieben, diese war wie folgt definiert:
``` python
def addieren(add1, add2):
	"In dieser Funktion addieren wir zwei Parameter."
	ergebnis = add1 + add2
	return ergebnis
```
Die Parameter add1 und add2 müssen auf jeden Fall beim Aufrufen der Funktion vorgeben werden.

### Standardparameter


``` python
def addieren_pflicht(add1, add2=5):
	"In dieser Funktion addieren wir zwei Parameter."
	ergebnis = add1 + add2
	return ergebnis
```

Werden Standardparameter wie in diesem Fall für den Parameter `add2` vorgegeben, so kann die Funktion auch nur mit einem Parameter aufgerufen werden.

``` python
addieren_pflicht(add1=2)
```

Das Ergebnis wäre in diesem Fall dann 7.


### Funktionsparameter mit unterschiedlicher Länge `*args`

Allgemein baut sich eine Funktion `*args` folgendermaßen auf:

``` python
def Funktionsname(*args) :
for i in args :
  return i
```

Eine Funktion, die beliebig viele Werte addiert, könnte mithilfe von `*args` sehr einfach gebaut werden.

``` python
def addieren_args(*args):
    ergebnis = sum(args)
    return ergebnis
```

Das Ergebnis der Funktion `addieren_args(5, 50, 45)` ist dann `100`.


### Schlüsselparameter (`**kwargs`)

Mit `**kwargs` ist Euch möglich, verschiedene Optionen einer Funktion auszuwählen bzw. zu aktivieren. In dem Fall von Rechnen ist dies z.B. Addieren und Subtrahieren. Eine Funktion, die dies beides kann ist, definiert sich wie folgt:

``` python
def addieren_kwargs(add1, add2, **kwargs):
    if kwargs.get("Rechenmethode") == 'Addieren':
           ergebnis = add1 + add2

    if kwargs.get("Rechenmethode") == 'Subtrahieren':
            ergebnis = add1 - add2

    return ergebnis
```

Dementsprechend liefert `addieren_kwargs(add1=5, add2=3, Rechenmethode='Addieren')` als Ergebnis `8` und `addieren_kwargs(add1=5, add2=3, Rechenmethode='Subtrahieren')` als Ergebnis `2`.

Natürlich lassen sich die verschiedenen Parameterarten beliebig miteinander kombinieren und setzen der Kreativität keine Grenzen.
