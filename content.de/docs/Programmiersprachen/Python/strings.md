---
title: "Strings"
date: 2020-05-25T20:55:27+02:00
draft: false
---

# Strings in Python

## Wie sehen Strings aus? 

Bevor ich auf den Hintergrund von Strings eingehe, kurze ein paar Beispiele wie Strings (Zeichenketten) aussehen:

```python
beispiel_string = 'String Nr.1'

beispiel_string_2 = "String Nr. 2"

langer_string = """
Do non aliquip non magna elit. Veniam consectetur veniam proident occaecat. Magna aliquip occaecat elit ad culpa. 
Anim labore id minim labore duis quis quis duis esse duis. 
Commodo commodo aute cillum adipisicing consequat irure ipsum velit anim aliquip. In eu voluptate reprehenderit qui culpa fugiat veniam in magna ut nisi duis. Tempor aliquip anim cupidatat non ullamco eu magna officia incididunt occaecat dolor pariatur id.
"""
```

## Motivation - Warum Strings? 

Der ein oder andere wird sich fragen, warum sollte man sich mit Strings auseinander setzen, schließlich erwartet man eher, dass Programmier-/Skriptsprachen wie Python darauf auslegt mit Zahlen zu arbeiten. Im Alltag sieht man dann, dass Strings doch häufiger vorkommen als man denkt. Beispiel sind: 

- Einlesen von Rohdaten: Wenn die Daten in keinem direkten (python-) kompatiblen Programm vorliegen kann, liest man die Datei Zeile für Zeile ein und dann sind die vorliegenden Daten als String abgespeichert.
- Webcrawling: Das Beispiel ist relativ ähnlich zu dem zuvor. Lädt man Daten von einer Website herunter, so sind die Daten häufig auch im String Format, da durch Browserrendering kein direkter Unterschied mehr zwischen "reinen" Zahlen sowie Strings gegeben ist.  
- Machine Learning: Im Bereich von Natural Language Processing sind Strings ein elementarer Baustein vom neuronalen Netz ohne sie könnte man z.B. keine automatischen Gedichte erstellen ;-) 
- Schreibt man längerer Python Skripte so kommt man um eine Dokumentation nicht herum, hier sind Strings unabkömmlich


## Arbeiten mit Strings

Das Erstellen von Strings ist einfach. In den oberen Beispiel habt ihr schon gesehen welche Arten von Strings es geben kann.

Es gibt;

- Strings mit einfachen Anführungszeichen `'`
- Strings mit dem klassischen Anführungszeichen `"`
- Strings mit dreifachen Anführungszeichen `"""`


### Standardstrings
 
Die Standard-Strings bestehen entweder aus dem `'` oder `"`. Nun fragt man sich, was sollte man eher benutzen. Die Website geeksforgeeks hat ein gutes [Beispiel](https://www.geeksforgeeks.org/single-and-double-quotes-python/) verwendet man die einfachen Anführungszeichen (Apostroph) so kann man keine weiteren Anführungszeichens in dem String verwenden. Im Deutschen kommt das weniger vor.

Beispiel:

```python
# Folgender String funktionier nicht
string_failure = 'gnisitricks's youtube channel'    
# Folgender String funktionier
string_success = "gnisitricks's youtube channel"    
```

### Strings über mehrere Zeilen

Möchte man bspw. eine sehr komplexe Funktion in seinem Program dokumentieren, so hat dieser in der Regel mehrere Funktionsargumente und man sollte die Funktionweise beschrieben.

Anhand des nachfolgenden Beispiels sieht man, dass es in diesem Anwendungsfall es sehr nützlich sein kann, wenn man Strings nicht nach einer Zeile beenden muss, sondern sie einfach weiter fortsetzen kann.

```python
def komplexe_funktion(arg1, arg2, arg3, **kwargs):
    """
    Diese Funktion erfordert folgende Argument:

    @params:

    - arg1: Argument1 wird benötigt da

    - arg2: Argument2 wird benötigt, um in der if schleife später die Laufvariable festzulegen

    - arg3: Argument3 definiert den Dateinamen

    - **kwargs: weitere Funktionsargumente können sein
    """
```

