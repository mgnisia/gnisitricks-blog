---
bookCollapseSection: true
weight: 1
---

# Python Einträge

- [Python Installation]({{< ref "python_setup.md" >}})

## Funktionen

- [Funktionen Teil 1]({{< ref "2018-02-25-Python_Funktionen.md" >}})
- [Funktionen Teil 2]({{< ref "2018-02-25-Python_Funktionen_teil2.md" >}})