---
title:  "Installation von Python auf Windows"
categories: ["Python" ]
tags: 
author: "Moritz Gnisia"
date: 2020-05-25
draft: false
---

## Video

{{< youtube KUCKVA-Uv5k >}}

## Warum sollte man mit Python als Programmiersprache anfangen?

- es ist eine sehr beliebte Programmiersprache (z.B. in Data Science)
- die Syntax ist sehr einfach
- Python is open source, d.h. sämtliche Implementierungen sind öffentlich verfügbar

### Welche Versionen von Python existieren?

Man kann grundsätzlich in zwei Python Versionen unterscheiden:

- Python2
- Python3 

Der Support für Python2 wurde [Anfang 2020 eingestellt](https://devguide.python.org/devcycle/#end-of-life-branches), daher sollte man auf jeden Fall Python3 installieren.

![](/images/python_version.png)


### Installationsmöglichkeiten von Python

Die einfachste Möglichkeit für die Installation von Python ist über [Anaconda](https://www.anaconda.com/products/individual), damit hat man eine vollfertige Python Installation und kann direkt los legen.

![](/images/python_setup.png)


### Installation von Anaconda

1.  Welche Art von Betriebssystem ist installiert 64bit / 32bit?

    1.  Rechtsklick auf Windows-Logo

    2.  Linksklick auf *System*

2.  Anaconda Website aufrufen\
    <https://www.anaconda.com/download/#windows>

3.  Je nach Betriebssystem entsprechende Installations-Datei
    herunterladen

4.  Installation durchführen

Python - Starten
================

### Welche Startmöglichkeiten existieren?

Python über Jupyter Notebook starten

1.  Programme aufrufen

2.  *Jupyter* eingeben

3.  Linksklick auf *Jupyter Notebook*

4.  Warten bis sich der Browser geöffnet hat

5.  Jupyter Notebook erstellen

    1.  *New*

    2.  *Python3*

6.  zum Beenden $\rightarrow$ Browser und Kommandozeile (CMD) schließen

Python über Spyder starten

1.  Programme aufrufen

2.  *Spyer* eingeben

3.  Linksklick auf *Spyder*

4.  zum Beenden $\rightarrow$ Fenster schließen

Python über CMD starten

1.  Installationspfad von Anaconda heraussuchen

2.  Installationspfad in die Zwischenablage kopieren $\rightarrow$
    STRG + C

3.  Kommandozeile starten

    1.  Rechtsklick auf Windows-Logo

    2.  Linksklick auf *Eingabeaufforderung*

4.  cd eingeben

5.  STRG + V drücken

6.  *python* eingeben

7.  *Enter* drücken

8.  zum Beenden $\rightarrow$ Fenster schließen

