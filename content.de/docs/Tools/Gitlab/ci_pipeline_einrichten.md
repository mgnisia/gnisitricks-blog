---
title: "Continuous Integration (CI) Pipeline in Gitlab einrichten"
author: "Moritz Gnisia"
date: 2021-01-03T08:26:28+02:00
draft: false
Categories:
 - Gitlab
 - Docker
 - Python
---

# Continuous Integration (CI) Pipeline in Gitlab einrichten

{{< youtube hVxBu8B56_Q >}}

## Agenda

1. Was ist Continuous Integration (CI) überhaupt? Was ist der Verwendungszweck?
2. Wie funktioniert eine CI Pipeline?
3. Was benötige ich für eine CI Pipeline in Gitlab? Welche Schritte sind für
notwendig?
4. Beispiel Projekt mit Python
5. Server Konfiguration

## Python Projekt

### Gitlab CI Yaml File

```yaml
stages:
  - test

python_test:
  stage: test
  image: python:3.9
  script:
    - pip3 install pipenv
    - pipenv install
    - pipenv run python3 -m pytest
```

## Server Konfiguration

### Server Update

```shell
# SSH Login
~ ssh root@IP-Adresse
# Update Server
root@server:~$ apt-get update && apt-get upgrade
```

### Neuen Benutzer anlegen

```shell
# Neuen Benutzer anlegen
root@server:~$ useradd -m -s /bin/bash youtube
# Passwort ändern
root@server:~$ passwd youtube
# Benutzer zu der sudo Gruppe  hinzufügen
root@server:~$ usermod -aG sudo youtube
```
### SSH Login für den root user deaktivieren  

```shell
# Bearbeiten der SSH Konfiguration
youtube@server:~$ sudo vim /etc/ssh/sshd_config
# In vim
/PermitRootLogin
yes mit no ersetzen
# Auf der Tastatur
:wq
# SSH Service neu starten
root@server:~$ sudo systemctl restart ssh
```

### Login ohne Passwort ermöglichen

```shell
# Auf deinem Laptop / PC
~ ssh-copy-id youtube@server-IP
# Login mit youtube ohne Passwort
# Installation von Docker
youtube@server:~$ sudo apt-get install -y docker.io
# Benutzer der docker Gruppe hinzufügen
root@server:~$ usermod -aG docker $USER
# Einmal aus und wieder einloggen
```

### Docker Installation

```shell
# Testen der Docker Installation
youtube@server:~$ docker ps
# Beispiel Container starten
youtube@server:~$ docker run hello-world
```

### Gitlab Runner installieren

```shell
# Gitlab Runner starten
# Quelle: https://docs.gitlab.com/runner/install/docker.html
youtube@server:~$ docker run -d --name gitlab-runner --restart
always -v /srv/gitlab-runner/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest
# Runner registrieren -> In gitlab.com oder deiner Gitlab Instanz
eintragen, Quelle: https://docs.gitlab.com/runner/register/
youtube@server:~$ docker run --rm -it -v
/srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner
register
```

## Downloads

- [Präsentation zum Herunterladen](/Gitlab_CI/2020_01_02_Gitlab_CI.pdf)
- [Python Projekt](/Gitlab_CI/Python_Projekt.zip)

## Hilfreiche Links

- https://docs.gitlab.com/ee/ci/README.html
- https://docs.pytest.org/en/stable/
- https://docs.docker.com/
- https://docs.gitlab.com/runner/install/docker.html 
- https://docs.gitlab.com/runner/register/

### Icons

- https://www.flaticon.com/authors/becr
- https://www.flaticon.com/autho
- https://www.flaticon.com/authors/freepik
  