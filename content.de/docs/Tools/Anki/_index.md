---
bookCollapseSection: true
weight: 3
---

# Anki Posts

- [**Automatisch Karteikarten erstellen**]({{< ref "automatisch_karteikarten_erstellen.md" >}})
- [**Automatisch Karteikarten erstellen - Erweiterung**]({{< ref "automatisch_karteikarten_erstellen_2.md" >}})