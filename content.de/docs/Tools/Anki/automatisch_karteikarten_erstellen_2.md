---
title: "Automatisch Karteikarten - Teil 2"
author: "Moritz Gnisia"
date: 2020-05-10T14:26:28+02:00
draft: false
Categories:
 - Anki
 - Excel
 - Imagemagick
---

# Automatisch Karteikarten mit Anki, Excel und Imagemagick erstellen - Teil 2

Gerne möchte ich an dieser Stelle eine kleine Erweiterung von meinem bisherigen [Artikel](automatisch_karteikarten_erstellen.de.md) vorstellen. Auf die Idee kam Norwid Behrnd, der mir die Unterlagen zu geschickt hat, nachdem er den anderen Artikel gelesen hatte. Vielen Dank für die schöne Erweiterung 😊 Mit dieser Vorlage könnt ihr auch Tags vergeben. Damit kann man seine Karteikarten bspw. nach Kategorien ordnen und nur diese lernen. Der Ablauf ist natürlich genauso wie im ersten Artikel nur könnt ihr noch eine andere Vorlage nehmen 👍

| Spalte1 | Spalte2 |  Spalte3 |
|------------|-----------------------------|---------|
| `foto1` | ```<img src=foto1.svg>``` |  `tag1` `tag2` |
| `foto2` | ```<img src=foto2.svg>``` |  `tag2`  |
| `foto3` | ```<img src=foto3.svg>``` |  `tag3` |

In Anki sieht das nach dem Import so aus:

![](/Anki/Extension/Tags.png)

Die Vorlage gibts natürlich zum hier [Download](/Anki/Extension/Vorlage.csv). Genauso wie das fertige [Anki Paket](/Anki/Extension/Tags.apkg).
