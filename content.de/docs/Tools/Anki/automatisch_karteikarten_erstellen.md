---
title: "Automatisch Karteikarten erstellen - Teil 1"
author: "Moritz Gnisia"
date: 2018-09-24T18:26:28+02:00
draft: false
weight: 10
Categories:
 - Anki
 - Excel
 - Imagemagick
---

# Automatisch Karteikarten mit Anki, Excel und Imagemagick erstellen
 

Wer kennt es nicht, während des Semesters haben sich viele Folien/digitalen Unterlagen & Co. in mehreren Fächern angesammelt 
und nun gilt es sich auf die anstehende Prüfung vorbereiten. Das Problem dabei häufig sind es so viele Folien,
dass man sich mehr damit auseinander setzt wie man die ganzen Unterlagen in ein passendes Format zum Lernen bringt, anstelle sich 
mit dem eigentlichen Lernen und den Inhalten auseinander zusetzen. Damit wir nicht alle :see_no_evil: so dasitzen, möchte ich Euch 
gerne mit diesem Artikel ein wenig helfen, Herr der Lage zu werden :smiley:, also legen wir los :muscle:!

Den ganzen Artikel habe ich auch in Video Form auf YouTube hochgeladen:

<div align="center"> 
<iframe width="560" height="315" src="https://www.youtube.com/embed/MF3VdTEnqvI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

## Anki

Wenn Ihr Fächer habt, in denen einfach sehr viel Theorie gelernt werden muss, und Ihr gerne mit Karteikarten lernt, ist Anki - meiner Meinung nach - ein super Programm, um Euch dabei zu helfen. Der Link zu Website [https://apps.ankiweb.net/](https://apps.ankiweb.net/). 
Mit Anki kann man nicht nur klassische Karteikarten erstellen, sondern auch noch: 

* Formeln in den Karteikarten einfügen
* Audio aufnehmen
* Fotos einfügen
* Karten mit Freunden/Kommilitonen teilen
* und einiges mehr ....

{{< figure src="https://camo.githubusercontent.com/5b74cbedf5bed2138122262b9b4eee9c205f4a6e/687474703a2f2f692e696d6775722e636f6d2f694a6962474d4c2e706e67" alt="Ankis Programm" caption="Screenshot Ankis" >}}


Wenn man schon viel Unterlagen in digitaler Form zu Verfügung hat, ist das umso besser, dann muss man Sie nicht mehr abschreiben. Ein weitere Vorteil ist, dass man die Karteikarten online synchronisieren kann und somit auch mobil gut lernen kann. 

##### Manuelle Erstellung von Karteikarten 

In der Vergangenheit habe ich häufig Karteikarten erstellt, indem ich mit Screenshot Tools wie z.B. dem Snipping Tool in Windows 
gearbeitet habe. Das Problem ist aber nun, wenn man einen Foliensatz von XXX Folien hat, ist man sehr lange mit dem Erstellen 
der Karteikarten beschäftigt, wie ich oben bereits geschreiben haben. In der Praxis sieht das dann häufig wie folgt aus:

{{< figure src="/anki_erstellen.gif" alt="Ankis Erstellen" caption="Prozess der Karteikartenerstellung" >}}


Der Prozess ist insgesamt also sehr mühselig und dauert viel zu lang :smiley::hourglass_flowing_sand:

## Automatische Erstellung der Karteikarten 

Der Prozess zum automatischen Erstellen der Karten läuft daher wie folgt ab:

1. Umwandeln von PDFs in Bilder z.B. jpeg, jpg oder png mit imagemagick 
2. Zuschneiden der Bilder in Fragen und Antworten mit Irfanview (Windows) oder z.B. Xnconvert (Mac)
3. Kopieren der Fotos in den Anki Medien Ordner
4. Erstellen einer CSV Tabelle mit der richtigen Zuordnung von Frage zu Antwort
5. Erstellen eines neuen Stapels in Anki
6. Import der CSV Tabelle im neuen Stapel


##### 1. Umwandeln von PDFs in Bilder

In der Regel liegen die meisten Vorlesungsunterlagen in Form von PDFs vor. Da man PDFs in ihrer ursprünglichen Form nicht einfach bearbeiten kann, muss man die Seiten der PDFs in einzelne Fotos umwandeln. Nach diesem Vorgang liegt nun also für jede Seite des PDF ein Bild vor. Bevor man mit der Umwandlung starten kann muss man zunächst imagemagick installiert haben: Die Installation von Imagemagick findest Du hier: 

<div align="center"> 
<iframe width="560" height="315" src="https://www.youtube.com/embed/pcnsmQ2kVV4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
Anschließend öffent man die Kommandozeile von Windows oder die Powershell und wechselt in das Verzeichnis der PDF Datei.

{{< figure src="/images/cd_windows.PNG" alt="Wechseln des Verzeichnis" caption="Wechseln in das Verzeichnis der PDF Datei" >}}

Die Umwandlung geht am einfachsten über imagemagick, der Befehl lautet: 

```bash
# Vorher sollte man mit cd in das Verzeichnis gewechselt sein
convert -density 300 Vorlesung_gesamt.pdf -background White Vorlesung_gesamt.jpg
```

Mit dem Befehl werden in dem zugehörigen Ordner dann Fotos von der jeder Seite der PDF Datei erstellt. Dies sieht im Explorer wie folgt aus:

{{< figure src="/images/convert_ergebnis.PNG" alt="Convert Ergebnis" caption="Ergebnis nach dem Umwandeln mit `convert`" >}}



##### 2. Zuschneiden der Bilder in Fragen und Antworten mit Irfanview/XnConvert

Mit Irfanview für Windows oder auf einem Mac mit XnConvert werden jetzt aus den ursprünglichen Fotos der PDF Seiten, die Fragen sowie Antworten für die späteren Karteikarten zugeschnitten. Der Zuschneideprozess ist nun sehr iterativ:

1. Wählt zunächst ein / zwei Probebilder
2. Legt Zuschnittmaße fest, einmal für die Frage und einmal für die Antwort

    Für die Definition der Zuschnittmaße der Bilder öffnet man ein oder zwei Fotos - um mögliche Abweichungen festzustellen - mit Irfanview. Anschließend wählt Ihr den Bereich für potentielle Frage also z.B. die Überschrift aus. In Ifranview werdet ihr in der Kopfzeile des Programms anschließend einen Bereich mit dem Titel _selection_ finden. Hier findet ihr die passenden Masse.

    {{< figure src="/Anki/Irfanview_Zuschnitt.png" alt="Zuschnitt Irfanview" caption="Zuschnitt Irfanview - Definition der Maße" >}}

    Für den Antwortbereich geht man analog vor. Man kann sich Maße notieren oder einfach im Zuschnittbereich die aktuelle Selektion auswählen, letzteres geht schneller :smile: Die Zahlen geben folgende Maße an:
    1. Zahl: `X-pos`
    2. Zahl: `Y-pos`
    3. Zahl: `Width`
    4. Zahl: `Height`

3. Öffnet nun den Bereich für die Stapelverarbeitung, dafür drückt ihr einfach die Taste `b`.
4. In dem Bereich `Work as` wählt ihr die Option `Batch conversion - Rename result files`
5. In dem Bereich `Batch conversion settings` wählt ihr die Option `use advanced options`, damit kann man die Zuschnittmaße festlegen
6. Klickt auf `Advanced`
7. Aktiviert den Menüpunkt `Crop`, wenn Eure Auswahl des Zuschnittsbereichs noch aktiv ist, kann man auf `Get current sel.` klicken und die Maße werden automatisch übernommen, ansonsten trag die Maße manuell ein.
    {{< figure src="/Anki/zuschnittmasse.PNG" alt="Zuschnitt Stapelverarbeitung" caption="Zuschnitt Irfanview - Stapelverarbeitung" >}} 
8. Im Bereich `Batch rename settings` trägt man das Muster nach dem die Bilder umbenannt werden sollen ein. Die Raute `#` steht dabei für eine fortlaufende Nummer, bei `###` ergibt sich somit eine dreistellige Nummer, die dann in Abhängigkeit der Fotos hochgezählt wird also 001, 002,.... Je nach Anzahl der Bilder kann man natürlich kleinerer oder größere Nummern wählen. Wählt für die zuzuschneidenden Fotos als Name z.B. `Antwort_###` oder `Frage_###`. Je nach Fach biete sich an unterschiedliche Namen zu verwenden, da ansonsten später beim Kopieren der Fotos die Datei überschrieben werden.
9. Klickt im Bereich `Output Folder` auf den Button `Use Current folder`
10. In der rechten obereren Bildhälfte findet Ihr einen Datei-Explorer, damit überhaupt Fotos umgewandelt werden, müssen diese zu den `Input files` hinzugefügt werden:
    1.  Dazu wählt entweder nur bestimmte Fotos aus und fügt diese mit dem Button `Add` hinzu
    2.  oder ihr wählt direkt alle fotos mit `Add all` aus. 
11. Die Konfiguration sollte nun ungefähr wie folgt aussehen:
    {{< figure src="/Anki/Irfanview_Konfiguration.PNG" alt="Konfiguration Irfanview" caption="Konfiguration Irfanview - Stapelverarbeitung" >}} 
12. Klickt nun auf `Start Batch` der Prozess der Stapelverarbeitung startet:
    {{< figure src="/Anki/Irfanview_Umwandeln.PNG" alt="Staptelverarbeitung" caption="Ablauf der Stapelverarbeitung" >}} 
13. Bewertet das Ergebnis des Zuschnitts und ändert ggf. die Maße
14. Erneutes Zuschneiden und wieder bei Punkt 2 anfangen, sobald man die Fragen hat, kann man den kompletten Prozess für die Antworten wiederholen.

Wenn man mit diesem Teilschritt fertig, sollte der Ordner mit den umgewandelten Fotos wie folgt aussehen:

{{< figure src="/Anki/Irfanview_Umwandeln_Fertig.PNG" alt="Umwandeln fertig" caption="Abgeschlossene Stapelverarbeitung" >}} 



##### 3. Kopieren der Fotos

Nun könnt ihr die Fotos in den Medien Ordner von Anki kopieren. Damit dieser Schritt funktioniert gibt es zwei Möglichkeiten entweder man geht über die Anki Einstellungen oder man öffnet den Ordner im Datei Explorer. Die erste Option klappt möglicherweise nicht, wenn noch keine Backups angelegt worden sind.

1. Option
   1. Anki öffnen
   2. Drückt `STRG+P` oder wählt die Einstellungen über den Menüpunkt `Extras -> Einstellungen` aus
   3. Klickt auf Tab `Sicherungskopien`
   4. Klickt auf den Button `Sicherungsordner öffnen` klickt
   5. Sobald sich der Ordner geöffnet hat, kann man eine Ordnerebene nach oben navigieren und man findet einen Ordner mit dem Namen `collection.media` in diesen müsst Ihr nun die Fragen und Antworten hinein kopieren.
   {{< figure src="/Anki/medienordner.png" alt="Umwandeln fertig" caption="Sicherungsordner öffnen" >}} 

2. Option - Falls die erste Option nicht klappt
   1. Öffnet den Dateiexplorer
   2. Aktiviert die Option `Ausgeblendete Elemente`
    {{< figure src="/Anki/Anki_hidden_files.PNG" alt="Explorer Einstellungen" caption="Windows Dateiexplorer - Ausgeblendete Elemente aktivieren" >}} 
   3. Navigiert in euer Benutzerverzeichnis also `C:\Users\Euer Benutzername`
   4. Dort angekommen findet Ihr einen Ordner `AppData`
   5. Geht in die Ordner `Roaming` dann in `Anki2` dann in das passende Benutzerprofil i.d.R. ist es der Ordner `Benutzer1` und dort findet ihr den Ordner `collection.media`, der vollständige Pfad sollte so ähnlich wie dieser aussehen: 
   `C:\Users\Moritz\AppData\Roaming\Anki2\Benutzer 1\collection.media`
   6. Nun verschiebt man die vorherigen zugeschnitten Fotos in den `collection.media ordner`


Nach dem Verschieben sollte der Ordner dann wie folgt aussehen (natürlich können dort noch mehr Fotos enthalten sein):

{{< figure src="/Anki/medienordner_bilder_verschoben.PNG" alt="Fotos verschoben" caption="Verschobene Fotos im `collection.media` Ordner" >}} 

##### 4. Erstellen der CSV Datei


Für das Erstellen der Lernkarten braucht man nun noch eine CSV-Datei. Hierzu kann man sich mit `Excel` einfach Abhilfe schaffen, ihr könnt Euch die Vorlage einfach [hier](/Anki/Vorlagen/Anki_Vorlage.xlsx) herunterladen.

1. Öffnet die Excel Datei
2. Je nachdem wie Ihr die Nummerierung in der Stapelverarbeitung eingestellt, müsst ihr ggf. das Padding (also die Anzahl der Nullen für einstelligen Werte) anpassen. Das geht über die Spalte `F` bzw. `K`.
    {{< figure src="/Anki/padding_excel.png" alt="Excel Einstellungen" caption="Padding Excel" >}}
    Die Funktion dieser Formel ist es, einen Textblock aus den verschiedenen Textelemente, die in den Spalten `C-E` für die Frage bzw. für die Antwort `H-I` zu erstellen.
3. Je nach Anzahl der erstellten Karteikarten muss man ggf. die Anzahl der Nummer in der Spalte `D` erhöhen. Anschließend könnt Ihr einfach die Zellen mit dem `+`-Symbol in der rechten unteren Ecke (s. Foto) nach unten ziehen.
 {{< figure src="/Anki/Excel_erweitern.png" alt="Excel erweiten" caption="Erweitern der Nummerierung" >}}
4. Erstellt eine neue Excel Datei und kopiert jeweils die Textblöcke für Fragen und Antwort aus den Spalten `F` und `K`. **Wichtig** beim Einfügen die Option: `Werte einfügen` verwenden
 {{< figure src="/Anki/Excel_Werte_einfuegen.png" alt="Excel Werte einfügen" caption="Werte Einfügen" >}} 
6. Die Excel Datei sieht dann folgendermaßen aus:
    {{< figure src="/Anki/CSV_Datei.png" alt="CSV Datei" caption="CSV Datei Vorlage" >}}
7. Anschließend speichert Ihr die Datei als `CSV`-Datei
    {{< figure src="/Anki/CSV_Datei_speichern.png" alt="CSV Datei" caption="CSV Datei Speichern" >}}

##### 5. Erstellen eines neuen Stapels in Anki

1. Öffnet Anki
2. Klickt auf `Stapel Erstellen`
3. Legt einen Namen für den Stapel fest

##### 6. Import der CSV Datei

1. Klickt auf `Datei importieren`
2. Wählt nun eure zuvor gespeicherte CSV Datei aus
3. Nun müsst Ihr noch die Option `HTML in den Felder zulassen` aktivieren
4. ggf. je nachdem wie die CSV Datei gespeichert ist, muss man anstelle des Semikolons ein Komma wählen
5. Wählt den Stapel aus zu dem ihr die Karteikarten hinzufügen wollt. Die Konfiguration sieht wie folgt nun aus:
    {{< figure src="/Anki/Anki_Config_Import.PNG" alt="Config Anki Import" caption="Konfiguration Anki Import" >}}

Wenn alles geklappt hat und ihr nun auf den jeweiligen Stapel klickt, findet Ihr dort Eure zugeschnitten Folien / Karteikarten. Viel Erfolg bei der Umsetzung und frohes Lernen :nerd_face: 

PS: Ich weiß, dass es sehr viele Schritte sind, aber ich denke, wenn man einmal den Ablauf verstanden hat, kommt man schnell rein! :blush:



