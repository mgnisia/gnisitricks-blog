---
title: Introduction
type: docs
disable_comments: true
layout: startpage
---

# Welcome to the Blog!

Welcome to my Blog! Here you find a broad range of different tutorials and solutions which deal with programming and software engineering. Maybe a suitable solution helps you :) Have fun reading it! You can also checkout the [german version](https://gnisitricks.de/de/).

<!-- My latest video:

{{< youtube tsK72kSgPoI  >}} -->
