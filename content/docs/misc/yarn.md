---
title: "Fixing yarn incompatibility"
date: 2019-12-03T21:23:51+01:00
draft: true
categories: 
    - Yarn
    - Node
---


# Fixing yarn incompatibility

## Problem


```txt
yarn create strapi-app my-project --quickstart
yarn create v1.22.10
[1/4] 🔍  Resolving packages...
[2/4] 🚚  Fetching packages...
error strapi-generate-new@3.4.3: The engine "node" is incompatible with this module. Expected version ">=10.16.0 <=14.x.x". Got "15.6.0"
error Found incompatible module.
info Visit https://yarnpkg.com/en/docs/cli/create for documentation about this command.
```

## Solution

