---
title: "Update one to many field with Graphql in GraphCMS"
date: 2021-03-20T21:23:51+01:00
draft: false
categories: 
    - Graphql
    - GraphCMS
    - OneToMany
---

# Update one to many field with Graphql in GraphCMS

Currently I am working on a migration from a Django based website to a replace it with [GraphCMS](https://graphcms.com/). As we have lots of entries in our database a manuel migration is not a feasible option and would lead to errors during the migration process. Therefore, I want to transfer the data between the two systems with standardized graphql / api calls. 

## Problem

The challenge I had was the following: In django we have a one-to-many field but I could not find the appropriate mutation wich updates the field correctly. You can do something like this (Please not that you have to customize the command for you GraphCMS schema): 

```
mutation MyMutation {
  __typename
  updateYourModel(data: {onetomanyfield: {set: {id: "xyz"}}}, where: {id: "xyz"}) {
    id
  }
}
```

but the problem is that you have afterwards just a one-to-one relationship. 

## Solution

What you want to do instead is the following:

1. First Mutation

```
mutation MyMutation {
  __typename
  updateYourModel(data: {onetomanyfield: {connect: {where: {id: "xyz"}}}}, where: {id: "xyz"}) {
    id
  }
}
```

2. Second Mutation 

```
mutation MyMutation {
  __typename
  updateYourModel(data: {onetomanyfield: {connect: {where: {id: "xyzabs"}}}}, where: {id: "xyz"}) {
    id
  }
}
```

The difference between the first mutation and second is the changed id field: `xyzabs`

## Example

![](/images/GraphCMS_multiple.png)

