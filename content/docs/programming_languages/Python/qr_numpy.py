import numpy as np
from scipy.linalg import norm

# Calculate Norm of first column vector
np.random.seed(10)
A = np.random.randint(-10, 10, (4,4))
a_1 = np.array([[-1],[-1],[1],[2]])
n_a1 = norm(a_1)

# Unit Vector
e_1 = np.zeros((4,1))
e_1[0,0] = 1

# Calculate || a_1 - ||a_1||_2 e_1 ||_2
n = norm(a_1 - n_a1*e_1)

# Calculate u_1
u_1 = 1/n * (a_1 - n_a1*e_1)

# Calculate H_1
H_1 = (np.eye(4) - 2*u_1 * u_1.transpose())

# New Submatrix A1
A1 = H_1.dot(A)

# Calculate A^1
A_1 = np.where( np.abs(A1) < 1e-15, 0, A1)


# Utilities
import pandas as pd
import clipboard
clipboard.copy(pd.DataFrame(A).to_latex())
clipboard.copy(pd.DataFrame(A_1).to_latex())


