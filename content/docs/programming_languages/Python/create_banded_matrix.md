---
title: "Creation of tridiagonal (banded) matrix with SciPy"
date: 2020-02-22
draft: false
categories: 
    - Python
---

# How to create a tridiagonal matrix?


## What is a tridiagonal matrix? 

It typically looks like this:

{{< katex display >}}
\begin{array}{cccccccc}
a_{11} & a_{12} & 0 & \cdots & \cdots & \cdots & \cdots & 0 \\
a_{21} & a_{22} & a_{23} & \ddots & & & & \vdots \\
0 & a_{32} & a_{33} & a_{34} & \ddots & & & \vdots \\
\vdots & \ddots & \ddots & \ddots & \ddots & \ddots & & \vdots \\
\vdots & & \ddots & \ddots & \ddots & \ddots & \ddots & \vdots \\
\vdots & & & \ddots & a_{76} & a_{77} & a_{78} & 0 \\
\vdots & & & & \ddots & a_{87} & a_{88} & a_{89} \\
0 & \cdots & \cdots & \cdots & \cdots & 0 & a_{98} & a_{99}
\end{array}
{{< /katex >}}


You can use the `toeplitz` function from the linear algebra library.

```python3
from scipy.linalg import toeplitz
toeplitz([2,-1,0,0,0],[0,-1,0,0,0])
```

The matrix you will obtain is a `5x5` Matrix with the following structure:




<!-- First of all we create a plain matrix with `np.eye`, in the next step we create matrix which has values only on the upper diagonal or the lower diagonal. Latter can be achieved with `np.diag`. Then you have your banded matrix.

```python
arr = np.eye(10)
arr += np.diag(np.random.randint(1,9,9),1)
arr += np.diag(np.random.randint(1,4,9),-1)
```

Another option is to use the [`diags`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.diags.html) function from scipy.

Rewriting the previous code would be identical to:

```python
from scipy.sparse import diags
sp = diags([[1]*10,np.random.randint(1,9,9),np.random.randint(1,4,9)],[0,1,-1])
# for printing
sp.toarray()
```

The advantage of this approach is the memory efficient storage. The amount of zero elements that is stored in `arr` is 72 that means that 28% of our storage is used efficiently the rest has no function at all. This is important as the matrix size is much more larger for different problems. -->