---
title: "Installation of IPython in a Virtualenv"
date: 2019-03-24T13:44:12+01:00
draft: false
author: "Moritz Gnisia"
categories:
    - Python
tags:
    - Virtualenv
    - Pipenv
    - Iphython
    - Development
---

Working with Virtualenvs like [pipenv](https://pipenv.readthedocs.io/en/latest/) is more than beneficial as you keep track easily of the different dependencies of your project and dont have to worry about your main python environment. However in during the development of your projects you might run into the situation that you require the shell for just verifying some small functions or run some scripts. The basic python shell provides is definitely suitable for this situation but using a shell like from [ipython](https://ipython.org/) is even better. You can select last commands with the arrow keys and also have a nice highlighting, but how do you setup the [ipython](https://ipython.org/) shell? Its straight forward:

1. Run `pipenv install ipython`
2. Run `python -m IPython`

There you go, thats it. Happy working!