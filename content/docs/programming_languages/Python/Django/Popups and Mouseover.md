---
title: "Adding Click- and Mouseover-Events simultaneously to GeoJson Markers on a Leaflet Map"
date: 2019-03-14T09:02:14+01:00
draft: false
categories: ['Leaflet']
tags: ['Javascript', 'Web Development', 'GeoJson']
---

When you are developing [Leaflet Maps](https://leafletjs.com/) for Desktop and Mobile Applications, you might want to add nice features like a _Mouse Over_ Effect for Desktop Users, but what if you have a mobile user also accessing the website in this case the easiest way might be just to add a click event. When I dealed with this problem, I found a good hint in this [Stackexchange](https://gis.stackexchange.com/questions/184661/how-to-make-both-hover-and-click-popup-work) with a top answer of [IvanSanchez](https://gis.stackexchange.com/users/53755/ivansanchez), where he mentioned that when you bind a popup you automatically also add and click event. Having this in mind you only need to add an mouseover event to your markers! Then you have successfully added the mouseover and click event.

The code can look like the following:

```Javascript
var markers = L.geoJson(collection, {
      onEachFeature: function(feature, layer) {
          layer.bindPopup(feature.properties.popupContent);
      },
        filter: function(feature, layer) {
            return feature.properties.some_filtering == "value_to_filter";
        },
      pointToLayer: function(feature, latlng) {
          return L.marker(latlng, {
          }).on('mouseover', function() {
            this.bindPopup(feature.properties.popupContent).openPopup();
          });
      }
  });

// Adding the markers to the map
markers.addTo(map);
```

