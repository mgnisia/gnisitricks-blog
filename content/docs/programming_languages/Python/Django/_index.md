---
bookCollapseSection: true
weight: 20
---

# Django Posts

- [**Accessing the values of a relationship field in a Django Dashboard**]({{< ref "Dashboard.md" >}})
- [**Click- and Mouseover-Events simultaneously to GeoJson Markers**]({{< ref "Popups%20and%20Mouseover.md" >}})