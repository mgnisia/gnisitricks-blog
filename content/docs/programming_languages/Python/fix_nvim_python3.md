---
title: "How to fix python3 support for nvim"
date: 2020-03-24T21:43:39+01:00
draft: false
categories:
    - Vim
---

If you want run into the problem of having no python3 support for `nvim`, I fixed the problem by running:

```python3
python3 -m pip install --user --upgrade pynvim
```

