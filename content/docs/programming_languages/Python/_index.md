---
bookCollapseSection: true
weight: 20
---

# Python Posts

1. [Setup IPython in a virtualenv]({{< ref "ipython.md" >}})