---
bookCollapseSection: true
weight: 3
---

# C++ - Posts

- [**A simple Class**]({{< ref "cpp_classes.md" >}})
- [**Zero initialized std::array**]({{< ref "cpp_classes.md" >}})