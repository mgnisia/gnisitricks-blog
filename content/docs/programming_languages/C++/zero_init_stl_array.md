---
title: "How to create a zero-initialized templated std::array?"
date: 2020-06-03T08:31:54+02:00
draft: false
slug: zero_initialized_stl_array
---

# How to create a zero-initialized templated std::array?

This morning I wanted to come up with a simple data structure in my master thesis in order implement a multigrid algorithm. As the latter is working with matrices or in a more general way tensors I wanted to use [templates](https://en.cppreference.com/w/cpp/language/templates) and the keyword `using` to improve the readability.

## New Data Structures

The new data structures are written quickly:

```c++
// File: datastructures.h
#include <array>
#ifndef DATASTRUCTURES_H

template<typename T1, int dim1, int dim2>
using matrix = std::array<std::array<T1, dim1>, dim2>;

template<typename T1, int dim>
using tensor3D = std::array<matrix<T1, dim, dim>, dim>;

#endif // !DATASTRUCTURES_H
```

## Initialization

Now the initialization is important:

```c++
// Non zero initialized
constexpr std::uint16_t dim = 8; 
tensor3D<double, dim> tensor_nz;

// Non zero initialized
tensor3D<double, dim> tensor_zeros = {};
```

The first example shows the creation of the variable but it is not using any [Aggregate Initialization](https://en.cppreference.com/w/cpp/language/aggregate_initialization). This was my first implementation, the attached data to this variable can also contain _non zero values_. If you are using the aggregate initialization then you get a zero initialized array. This examples shows how a small inaccuracy can have a big impact on your calculations. This [stackoverflow Post](https://stackoverflow.com/a/18304935/7825803) gives you further information about this topic and also clarifies between the _default initialization_ and _zero initialization_.