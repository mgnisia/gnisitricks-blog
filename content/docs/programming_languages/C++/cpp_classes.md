---
title: "A simple class"
date: 2018-12-04T19:59:12+01:00
draft: false
categories:
    - C++
slug: cpp_simple_class
---
#### Definition of the class 

The first step to write a class in `C++` is by typing the keyword `class` and the corresponding name of the class like so

```cpp
class Airplane {
    // Attributes and Methods of the class come here
}
```

In the next step you can define three different type of attributes to this class:

* private
* protected
* public

The main difference between the public and the two other attributes are the aspect that you can't access from the outside. This might be helpful in terms of security reasons. We will just take the public attributes.

#### Attributes and Methods

With defining the class we need to add some more functionality to it. We achieve this by defining attributes and our methods for the class. In our example the `Airplane` we will just take the following attributes:

* Number of Engines
* V_max
* V_min
* Name of the Airplane

As all attributes are know prior to creating the class we will a keep them public. Lets fill the body of our class template:

```cpp
class Airplane {
   public:
    int number;
    float v_max;
    float v_min;
    std::string airline;
}
```

The next aspect for creating the class is setting up the constructor for the class in order to ensure that when we create an instance of our class all variables are assigned correctly.
To create the constructor you simply write a _function_ with the name of the class so in this case `Airplane`. However the function differs a little bit from the typical appearance like

```cpp
int add(int num_1, int num_2) {
    return num_1 + num_2;
}
```

The constructor function looks like this in general:

```cpp
// General layout of the class constructor
classname(int attribute_1, int attribute_2, ...): class_attribute_1(attribute_1), class_attribute_2(attribute_2) {
    // additional task you can perform with the constructor
    // e.g. print out that the class has been created
}
```

So let's add the constructor to our class as well.

```cpp
class Airplane {
   public:
    int number;
    float v_max;
    float v_min;
    std::string airline;
    Airplane(int num, float vmax, float vmin, std::string name): number(num), v_max(vmax),
    v_min(vmin), airline(name) {
        std::cout << "An instance of the airplane class has been created" << std::endl;
    }
}
```

No we can easily create new objects of and access the objects of them:

```cpp
#include "iostream"

class Airplane {
   public:
    int number;
    float v_max;
    float v_min;
    std::string airline;
    Airplane(int num, float vmax, float vmin, std::string name): number(num), v_max(vmax),
    v_min(vmin), airline(name) {
        std::cout << "An Instance of the airpline has been created!" << std::endl;
    }
};

int main(int argc, char const *argv[])
{
    Airplane a_1(2, 550, 120, "Fly me to the moon");
    std::cout << "This number of engine of Airplane a_1 is " <<  a_1.number << std::endl;
    return 0;
}
```

Now one can add more functionality to the class with more functions like a _Copy Constructor_ or a _Destructor_. Of course you can also write just simple methods like in the following:


```cpp
#include "iostream"

class Airplane {
   public:
    int number;
    float v_max;
    float v_min;
    std::string airline;
    Airplane(int num, float vmax, float vmin, std::string name): number(num), v_max(vmax),
    v_min(vmin), airline(name) {
        std::cout << "An Instance of the airpline has been created!" << std::endl;
    };
    void speeds() {
        std::cout << "The minimum speed is " << v_min << std::endl;
        std::cout << "The maximum speed is " << v_max << std::endl;
    };
};

int main(int argc, char const *argv[])
{
    Airplane a_1(2, 550, 120, "Fly me to the moon");
    a_1.speeds();
    return 0;
}
```