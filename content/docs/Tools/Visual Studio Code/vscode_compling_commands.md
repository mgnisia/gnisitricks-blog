---
title: "How to use VSCode for writing compiler commands"
date: 2019-09-27T11:32:33+02:00
draft: false
Categories:
 - C
 - VScode
 - compiler
bookCollapseSection: true
---


## How to use VSCode for writing compiler commands

{{< figure src="/Compiler_Commands_VScode.gif" alt="Complier Commands" caption="Simplify writing compiler commands" >}}


### Motivation

While learning C/C++ I found that writing the compiler commands for the terminal is tedious job especially if you write many files. To simplify this process you can use the snippets of [vscode](https://code.visualstudio.com/). 

## Solution

The steps are as follows:

1. Open the [command palette](https://code.visualstudio.com/docs/getstarted/tips-and-tricks#_command-palette)
2. Enter `Configure User Snippet`
3. Select the language for which you would like to configure your the snippets, I choose `C`
4. A `.json` File will open and you can configure your snippets for all `C` programms
5. For writing the compiler commands I use the following snippet: 
```json
   "compile_pthread": {
		"prefix": "comp",
		"body": [
			"$LINE_COMMENT gcc -o $TM_FILENAME_BASE -lpthread $TM_FILENAME && ./$TM_FILENAME_BASE ",
		],
		"description": "Create compiler commands"
    }
```
With the prefix `comp` I can write the compiler command for a specific c file. With the variable `$LINE_COMMENT` I create a line comment, `$TM_FILENAME_BASE` takes the file name and `$TM_FILENAME` is the full file name e.g. `programm.c`. I included the `-lpthread` option as I currently study parallel programming. 



