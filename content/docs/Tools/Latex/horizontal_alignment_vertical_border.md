---
title: "How to fix a wrong horizontal aligned column border in a multicolumn table in LaTex?"
date: 2020-07-09T18:21:28+02:00
draft: false
categories:
    - LaTex
    - Multicolumn
    - Alignment
---

# How to fix a wrong horizontal aligned column border in a multicolumn table?

## Problem

I just had the following problem in LaTex: 

![](/images/wrong_aligend_column_border.png)

As you can clearly see in the image the border of the right column in the first row is not aligned with the border in the rows below.

The LaTex code for this error is:

```latex
% table excerpt
Class Type & \multicolumn{3}{c}{A} & \multicolumn{3}{|c}{B}
```
For simplicities reasons I only show a small except of the table.

## Solution

To fix this problem just move the vertical bar `|` to the other column.

```latex
% table excerpt
Class Type & \multicolumn{3}{c}{A|} & \multicolumn{3}{|c}{B}
```

Afterwards the alignment is correct again:

![](/images/wrong_aligend_column_border_fixed.png)
