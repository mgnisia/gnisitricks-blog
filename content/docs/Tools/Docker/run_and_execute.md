---
title: "Run/Start Docker Container and execute command"
date: 2020-09-03T08:47:41+02:00
draft: false
---

## Run/Start Docker Container and execute command

In some situations you have to test different commands inside a docker container but you might not want to keep running in the background. Then you can use the following command which: 

- will start the container with `docker_image`
- execute a command specified after the `-c` argument

```shell
docker run --rm -v `$(pwd)`:/temp_volume docker_image \
/bin/bash -c "cd /temp_volume && python3 script.py"
```
