---
title: "How to open Clion straight out of the terminal"
date: 2019-11-28T08:16:20+01:00
draft: false
categories: 
    - C
    - C++
---

# How to open Clion straight out of the terminal

With the `open` command in MacOS you can open applications directly from the terminal, if you are a in a certain directory and you would like to open it in [clion](https://www.jetbrains.com/clion/) without starting clion first and then select the directory again, you can just run `open -a Clion .` 😊
