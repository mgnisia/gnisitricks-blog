---
title: "Rearrange Latex Content"
date: 2020-06-22T10:30:18+02:00
draft: false
tags:
    - latex
    - optimization 
images:
    - /images/exam_sheet_annotated.png
---


# How to rearrange LaTex more content easily?

As a engineering student you will be exposed at some point during your studies to LaTex. This is probably only a matter of time ;-) I enjoy working with Latex even though it can be quite frustrating from time to time. 

## Challenge

Recently I had the following challenge: I wanted to create an exam sheet for a course with Latex. Writing such a sheet is more work than writing it immediately by hand however it also allows you once done to rearrange topics and annotate them. The number of iterations you can do with a hand written sheet are definitely limited. If you are wondering what a cheat sheet can look like I mean something like this:

![](/images/exam_sheet.png)

The problem of latex and the iteration can be explained with this picture: 

![](/images/exam_sheet_annotated.png)

If you realise during your study progress that you would like to exchange or rearrange some topics on your sheet than you have to move all the latex text to this different column. I was not able to find a easy "drag and drop" solution where you could select this area and move it somewhere else.

## Drag and Drop - Solution

Then I came across [Xournal++](http://xournal.sourceforge.net/) there you can create empty notes and have also the possibility to insert latex snippets. So you can still write you sheet there if you don't mind having limited amount of formating tools in terms of font and so on you can rearrange your content a lot more easier.

This gif shows you the process:

![](/images/xournal_move.gif)