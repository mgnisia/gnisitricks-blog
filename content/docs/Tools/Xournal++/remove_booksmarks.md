---
title: "How to remove booksmarks from PDFs?"
date: 2020-06-29T08:22:14+02:00
draft: false
---

# Removal of booksmarks in PDFs

1. Open Xournal++
2. Open the PDF
3. Export the PDF again
4. Now the bookmarks are removed


