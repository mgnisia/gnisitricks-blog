---
title: "Create Continuous Integration (CI) Pipeline in Gitlab"
author: "Moritz Gnisia"
date: 2021-01-03T08:26:28+02:00
draft: false
Categories:
 - Gitlab
 - Docker
 - Python
---

# Create Continuous Integration (CI) Pipeline in Gitlab

{{< youtube 6QtJDaycUwA  >}}

## Agenda

1. What is Continuous Integration (CI)? What it is the application?
2. How does a CI Pipeline work?
3. What do I need for a CI Pipeline? What steps are necessary?
4. Small Project in Python
5. Server Configuration


## Python Project

### Gitlab CI Yaml File

```yaml
stages:
  - test

python_test:
  stage: test
  image: python:3.9
  script:
    - pip3 install pipenv
    - pipenv install
    - pipenv run python3 -m pytest
```

## Server Configuration

### Server Update

```shell
# SSH Login
~ ssh root@IP-Adresse
# Update Server
root@server:~$ apt-get update && apt-get upgrade
```

### Create new user

```shell
# Create new user
root@server:~$ useradd -m -s /bin/bash youtube
# Change Password
root@server:~$ passwd youtube
# Add user to sudo Group
root@server:~$ usermod -aG sudo youtube
```
### Disable root access per ssh

```shell
# Edit SSH Config
youtube@server:~$ sudo vim /etc/ssh/sshd_config

# inside vim 
/PermitRootLogin
yes mit no ersetzen

# on your keyboard
:wq
# Restart ssh service
root@server:~$ sudo systemctl restart ssh
```

### Password-less login + Installation of Docker

```shell
# on your Laptop / PC
~ ssh-copy-id youtube@server-IP
# Login mit youtube ohne Passwort

# Installation of Docker
youtube@server:~$ sudo apt-get install -y docker.io

# Adding user to docker group
root@server:~$ usermod -aG docker $USER

# Logout and login again
```

### Checking of docker installation

```shell
# Testen der Docker Installation
youtube@server:~$ docker ps
# Beispiel Container starten
youtube@server:~$ docker run hello-world
```

### Install Gitlab Runner

```shell
# Start Gitlab Runner
# Resource: https://docs.gitlab.com/runner/install/docker.html
youtube@server:~$ docker run -d --name gitlab-runner --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest

# Register Gitlab Runner-> write gitlab.com oder your Gitlab Instanz Resource: https://docs.gitlab.com/runner/register/
youtube@server:~$ docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```

## Downloads

- [Presentation](/static/Gitlab_CI/2021_01_09_Gitlab_CI_EN.pdf)
- [Python Project](/Gitlab_CI/Python_Projekt.zip)

## Helpful Links

- https://docs.gitlab.com/ee/ci/README.html
- https://docs.pytest.org/en/stable/
- https://docs.docker.com/
- https://docs.gitlab.com/runner/install/docker.html 
- https://docs.gitlab.com/runner/register/

### Icons

- https://www.flaticon.com/authors/becr
- https://www.flaticon.com/autho
- https://www.flaticon.com/authors/freepik
  