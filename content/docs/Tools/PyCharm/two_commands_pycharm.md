---
title: "Entering two commands in PyCharm simultaneously"
date: 2019-11-15T11:16:08+01:00
draft: false
Categories:
    - Python
    - Pycharm
---

## Entering two commands in PyCharm simultaneously

When you are working with matplotlib the default workflow is two create first the image itself with `plt.plot()` and then `plt.show()`. Those commands are usually entered after each other. However you can also enter them simultaneously by just concatenating them with a semicolon. 

So you can run `plt.show([1,2,3]); plt.show()` and the image if you run PyCharm in the scientific mode will popup immediately.

