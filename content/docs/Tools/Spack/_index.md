---
bookCollapseSection: true
weight: 2
---

# Overview of Spack Entries

- [Fixing Gcc Setup]({{< ref "Fixing_Setup_Spack.md" >}})
- [Python3.5 Setup]({{< ref "Fixing_Setup_Spack.md" >}})