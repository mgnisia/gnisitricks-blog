---
title: "Installation Problem of gcc with Spack"
date: 2020-04-03T07:21:35+02:00
draft: false
categories: 
    - Spack
---

While running through the setup of [Spack](https://github.com/spack/spack/) on MacOS I faced the following problem while running `spack install gcc@8.2.0`

in a short form it was a error message like:

```shell
# /usr/include was missing
```

Drawback of the following solution:

- compiler settings have to be remapped 
- had to reinstall my previous modules gain, as the compiler settings changed

![](/images/spack_setup.png)

My solution:

1. went to the spack-root folder 
2. `git pull`
3. `spack compiler find`
