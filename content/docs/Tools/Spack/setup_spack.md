---
title: "Introduction to Spack"
date: 2020-04-03T07:21:35+02:00
draft: true
categories: 
    - Spack
---

The first contact I had with [Spack](https://github.com/spack/spack/) was during my first HPC lab course at TUM. THE [LRZ](https://www.lrz.de/english/) uses it on the different HPC computers in order to load the different modules which are required for different setups of programs e.g. one application might require the latest MPI version with a specific g++ or gcc compiler. If you already tried to install those different versions manually you can figure out that this becomes a challenge only for a few dependencies. The main advantage of using spack I see so far is that you can eas After using [Docker](https://docker.com/) for those different setups

