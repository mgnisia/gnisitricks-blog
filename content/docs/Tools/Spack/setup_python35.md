---
title: "Fixing openssl/ssl dependency for python 3.5.0"
date: 2020-05-14T07:05:37+02:00
draft: false
slug: openssl-python35-setup-spack
categories:
    - spack
    - tox
    - ci
---

Yesterday I spent couple of hours on how to fix the following error:

```shell
import _ssl             # if we can't import it, let the error propagate
ModuleNotFoundError: No module named '_ssl'
```

The error popped up during the setup process of a docker container for CI pipeline which should test a python cli tool with [tox](https://tox.readthedocs.io/en/latest/). The problem could easily be rooted to a openssl library dependency. I also tried the setup with `pyenv` which is an easy python environment tool but with this approach the same errors occurred. As I was already quite familiar with [spack](https://spack.readthedocs.io/) I thought how that it might be possible to fix it the ssl problem with this tool. But in the beginning it also didn't worked out. On github you find a long [issue](https://github.com/pyenv/pyenv/issues/950) in the pyenv repo which describes on how to setup the different `CFLAGS` and `LDFLAGS`. There I also found a good tip from [Robby Dyer](https://github.com/robbydyer) who wrote that if you have python version less than 3.5.2 it needs openssl 1.0.

When you run `spack spec python@3.5.0`. (Spack version is `0.14.2`)

```shell
Input spec
--------------------------------
python@3.5.0

Concretized
--------------------------------
python@3.5.0%gcc@7.5.0+bz2+ctypes+dbm~debug+libxml2+lzma~nis~optimizations+pic+pyexpat+pythoncmd+readline+shared+sqlite3+ssl~tix~tkinter~ucs4~uuid+zlib arch=linux-ubuntu18.04-haswell
    ^bzip2@1.0.8%gcc@7.5.0+shared arch=linux-ubuntu18.04-haswell
        ^diffutils@3.7%gcc@7.5.0 arch=linux-ubuntu18.04-haswell
            ^libiconv@1.16%gcc@7.5.0 arch=linux-ubuntu18.04-haswell
    ^expat@2.2.9%gcc@7.5.0+libbsd arch=linux-ubuntu18.04-haswell
        ^libbsd@0.10.0%gcc@7.5.0 arch=linux-ubuntu18.04-haswell
    ^gdbm@1.18.1%gcc@7.5.0 arch=linux-ubuntu18.04-haswell
        ^readline@8.0%gcc@7.5.0 arch=linux-ubuntu18.04-haswell
            ^ncurses@6.1%gcc@7.5.0~symlinks~termlib arch=linux-ubuntu18.04-haswell
                ^pkgconf@1.6.3%gcc@7.5.0 arch=linux-ubuntu18.04-haswell
    ^gettext@0.20.1%gcc@7.5.0+bzip2+curses+git~libunistring+libxml2+tar+xz arch=linux-ubuntu18.04-haswell
        ^libxml2@2.9.9%gcc@7.5.0~python arch=linux-ubuntu18.04-haswell
            ^xz@5.2.4%gcc@7.5.0 arch=linux-ubuntu18.04-haswell
            ^zlib@1.2.11%gcc@7.5.0+optimize+pic+shared arch=linux-ubuntu18.04-haswell
        ^tar@1.32%gcc@7.5.0 arch=linux-ubuntu18.04-haswell
    ^libffi@3.2.1%gcc@7.5.0 arch=linux-ubuntu18.04-haswell
    ^openssl@1.1.1d%gcc@7.5.0+systemcerts arch=linux-ubuntu18.04-haswell
        ^perl@5.30.1%gcc@7.5.0+cpanm+shared+threads arch=linux-ubuntu18.04-haswell
    ^sqlite@3.30.1%gcc@7.5.0~column_metadata+fts~functions~rtree arch=linux-ubuntu18.04-haswell
```

you see that the openssl dependency is already related to a version `1.1.1`. Having in mind the previous tip this obviously doesn't match. Therefore I tried to install python@3.5.0 with a fixed dependency to openssl `1.0.1`. Therefore I look at the available openssl versions with `spack info openssl`, the last openssl version was `1.0.2t` To install python with this openssl version run: 

```shell
spack install python@3.5.0 ^openssl@1.0.2t
```

The `^` (circumflex) defines the dependency to openssl. With this setup it worked smoothly 😊 Afterwards you can run `spack load python@3.5.0` Happy coding! 