---
title: "Create markdown tables in the terminal"
date: 2019-11-27T13:02:48+01:00
draft: false    
categories: 
    - Terminal
---

Functions are definitely a nice tool in [fish](https://fishshell.com/) to help you work more efficient and utilize them in the daily life. Recently during a lab course one had to describe the function of different `cpp`-files. As Markdown is the way to comment them in gitlab or github I thought that it might be beneficial to have the possibility to create a markdown table just in the terminal. In fish you can easily write your function or if you are more familiar with bash, just use the latter. A brief description of the script: 

1. Create a variable which contains the header of the table (in my case just two columns)
2. Iterate through the desired file type, here it's `cpp` files
3. Append the new line to the `header` variable
4. use `printf` to get a nice output

Fish Function

```bash
function md_table
  set header "| header | header |\n| ------ | ------ |\n"
  for i in *.cpp;
    set -a header "| $i |  |\n";
  end;
  printf "$header";
end
```

Bash Script

```bash
#!/usr/bin/env bash
header="| header | header |\n| ------ | ------ |\n";
for i in *sh; do;
    header+="| ${i} |  |\n";
done;
printf "$header"
```