---
bookCollapseSection: true
weight: 25
---

# Markdown Posts

- [**Create a Markdown Table in the Terminal (Fish shell)**]({{< ref "create_markdown_table_function.md" >}})