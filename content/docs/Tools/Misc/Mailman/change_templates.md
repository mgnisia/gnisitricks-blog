---
title: "Changing Templates in Mailman (Setup with Mailcow)"
date: 2020-04-04T07:27:12+02:00
draft: false
categories:
    - Mailman
---

## Changing Templates in Mailman

### Why Mailman?

Everybody who has worked with [Mailman]() knows how easy you can manage your mailing lists. Every other newsletter tool is most of the times a lot more complicated: you have to login, create a template, write the email, select the recipients and then send it. In Mailman you can simply open your mail account write an email to an specific email address list@example.com and it will be forwarded.

### Setup

The setup I ran Mailman for my voluntary project [Einmal ohne, bitte](https://einmalohnebitte.de) is as follows:

![](/images/mailman_setup.svg)

I used it in combination with [Mailcow](https://github.com/mailcow/mailcow-dockerized/) which is a really nice plug and play solution. The setup in combination with mailman can be a little be tedious.

### Changing the default Email templates - if it doesn't works via Postorius

Yesterday I wanted to change the templates for a email list. After having changed them in Postorius new subscribers couldn't be added. Therefore I was wondering if there is another solution. So every template has some default lookup pattern which template to be used when. An Overview of those templates can be found in the [Gitlab](https://gitlab.com/mailman/mailman/-/tree/master/src/mailman/templates/) of mailman. Instead of changing the email templates in postorius you can change them directly in mailman. The approach is:

1. Navigate to the templates folder in mailman
2. Create a new folder `lists`
3. Create a new folder `prefix.domain-examples.com` this is for you actually email address. In this case the email address would be prefix@domain-examples.com or for a subdomain prefix@lists.domain-examples.com -> foldername `prefix.lists.domain-examples.com`
4. Create folders you languages (in my case it was `de` for German and `en` for English)
5. Create a text file like in the original templates called e.g. `list:user:notice:welcome.txt`.

```shell
templates/
├── domains
└── lists
    └── prefix.domain-examples.com
        ├── de
        │   └── list:user:notice:welcome.txt
        └── en
            └── list:user:notice:welcome.txt
```

You can of course change the the other templates as well. Inside the txt file you can use the different variables e.g:

- $listname
- $request_email

