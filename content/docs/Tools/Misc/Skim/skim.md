---
title: "Opening PDFs in the same tab in Skim"
date: 2019-11-06T14:06:51+01:00
draft: false
categories: 
 - Pdf
 - Skim
---

## Opening PDFs in the same tab in Skim

Skim is a nice and useful programm for PDF annotations. However if you open multiple PDFs, macOS always opens them in new Skim windows. If you want to change this behavior run the following command:

```bash
defaults write -app Skim AppleWindowTabbingMode -string always
```

There are different options available and you can find a [list here](https://sourceforge.net/p/skim-app/wiki/Hidden_Preferences/)


