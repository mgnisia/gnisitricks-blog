---
bookCollapseSection: true
weight: 50
---

# Posts about various other tools

- [Mailman]({{< ref "Mailman/_index.md" >}})
- [Skim]({{< ref "Skim/skim.md" >}})