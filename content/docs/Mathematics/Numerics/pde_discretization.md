---
title: "Discretization of the Possion Equation"
author: "Moritz Gnisia"
date: 2020-06-28
draft: false
---
## Discretiazation of a PDE

You might be wondering where such kind of matrix structures appear. A simple example is the discretization of PDE (partial differential equations) such as:

{{< katex display >}}
-\nabla u = f(x)
{{< /katex >}}

If we consider a discretization with finite differences you can approximate your {{< katex inline >}}u_i{{< /katex >}} as:

{{< katex display >}}
u''(x) \approx \frac{u_{i-1} - u_i - u_{i+1}}{h^2}
{{< /katex >}}.

In a 2D setting you can apply the same discretization of course for the `y`-direction in the same manner.

![](/images/grid.svg)
